import { sign } from 'jsonwebtoken'
import { DbConnection } from "../../../interfaces/DbConnectionInterface"
import { UserInstance } from "../../../models/UserModel"
import { JWT_SECRET } from '../../../utils/utils'
import { compose } from '../../composable/composable.resolver'

export const tokenResolvers = {
  Mutation: {
    createToken: compose()((parent, {email, password}, {db}: {db: DbConnection}) => {
      return db.User.findOne({
        where: { email: email },
        attributes: ['id', 'password']
      }).then((user: UserInstance) => {
        const errorMessage: string = 'Unauthorized, wrong e-mail or password.'
        
        if (!user || !user.isPassword(user.get('password'), password)) {
          throw new Error(errorMessage)
        }

        const payload = { sub: user.get('id') }

        return {
          token: sign(payload, JWT_SECRET, {
            expiresIn: '1d'
          })
        }
      })
    })
  }
}
