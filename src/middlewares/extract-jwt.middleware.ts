import { RequestHandler, Request, Response, NextFunction } from "express"
import { verify } from 'jsonwebtoken'
import { JWT_SECRET } from "../utils/utils"
import { UserInstance } from "../models/UserModel"
import db from './../models'

export const extractJwtMiddleware = (): RequestHandler => {
  return (req: Request, res: Response, next: NextFunction): void => {
    const authorization: string = req.get('authorization') // Authorization = Bearer
    const token: string = authorization
      ? authorization.split(' ')[1]
      : null

    req['context'] = {}
    req['context']['authorization'] = authorization

    if (!token) {
      return next()
    }

    verify(token, JWT_SECRET, (err, payload: any) => {
      if (err) {
        return next()
      }

      db.User.findById(payload.sub, {
        attributes: ['id', 'email']
      }).then((user: UserInstance) => {
        if (user) {
          req['context']['authUser'] = {
            id: user.get('id'),
            email: user.get('email')
          }
        }

        return next()
      })
    })
  }
}
