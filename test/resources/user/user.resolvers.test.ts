import { sign } from 'jsonwebtoken'
import { app, chai, db, expect, handleError } from './../../test-utils'
import { JWT_SECRET } from '../../../src/utils/utils'
import { UserInstance } from '../../../src/models/UserModel'

describe('User', () => {
  let token: string
  let userId: number

  beforeEach(() => {
    return db.Comment.destroy({where: {}})
      .then((rows: number) => db.Post.destroy({where: {}}))
      .then((rows: number) => db.User.destroy({where: {}}))
      .then((rows: number) => db.User.bulkCreate([
        {
          name: 'Marcel',
          email: 'marcelbrilha@gmail.com',
          password: '123'
        },
        {
          name: 'Mocha user',
          email: 'mocha1@gmail.com',
          password: '123'
        },
        {
          name: 'Mocha user2',
          email: 'mocha2@gmail.com',
          password: '123'
        }
      ]))
      .then((users: UserInstance[]) => {
        userId = users[0].get('id')
        const payload = {
          sub: userId
        }
        token = sign(payload, JWT_SECRET)
      })
  })

  describe('Querys', () => {
    describe('application/json', () => {
      describe('users', () => {
        it('should return a list of users', () => {
          const body = {
            query: `
              query {
                users {
                  name
                  email
                }
              }
            `
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              const usersList = response.body.data.users

              expect(response.body.data).to.be.an('object')
              expect(usersList).to.be.an('array')
              expect(usersList[0]).to.not.have.keys(['id', 'photo', 'createdAt', 'posts'])
              expect(usersList[0]).to.have.keys(['name', 'email'])
            })
            .catch(handleError)
        })

        it('should paginate a list of users', () => {
          const body = {
            query: `
              query getUsersList($first: Int, $offset: Int) {
                users(first: $first, offset: $offset) {
                  name
                  email
                  createdAt
                }
              }
            `,

            variables: {
              first: 2,
              offset: 1
            }
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              const usersList = response.body.data.users

              expect(response.body.data).to.be.an('object')
              expect(usersList).to.be.an('array').of.length(2)
              expect(usersList[0]).to.not.have.keys(['id', 'photo', 'posts'])
              expect(usersList[0]).to.have.keys(['name', 'email', 'createdAt'])
            })
            .catch(handleError)
        })
      })

      describe('user', () => {
        it('should return a single users', () => {
          const body = {
            query: `
              query getSingleUser($id: ID!) {
                user(id: $id) {
                  id
                  name
                  email
                  posts {
                    title
                  }
                }
              }
            `,

            variables: {
              id: userId
            }
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              const singleUser = response.body.data.user

              expect(response.body.data).to.be.an('object')
              expect(singleUser).to.be.an('object')
              expect(singleUser).to.have.keys(['id', 'name', 'email', 'posts'])
              expect(singleUser.name).to.equal('Marcel')
              expect(singleUser.email).to.equal('marcelbrilha@gmail.com')
            })
            .catch(handleError)
        })

        it('should return only name attribute', () => {
          const body = {
            query: `
              query getSingleUser($id: ID!) {
                user(id: $id) {
                  name
                }
              }
            `,

            variables: {
              id: userId
            }
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              const singleUser = response.body.data.user

              expect(response.body.data).to.be.an('object')
              expect(singleUser).to.be.an('object')
              expect(singleUser).to.have.key('name')
              expect(singleUser.name).to.equal('Marcel')
              expect(singleUser.email).to.be.undefined
            })
            .catch(handleError)
        })

        it('should return an error if user not exists', () => {
          const body = {
            query: `
              query getSingleUser($id: ID!) {
                user(id: $id) {
                  name
                  email
                }
              }
            `,

            variables: {
              id: -1
            }
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              expect(response.body.data.user).to.be.null
              expect(response.body.errors).to.be.an('array')
              expect(response.body).to.have.keys(['data', 'errors'])
              expect(response.body.errors[0].message).to.equal('Error: User with id: -1 not found.')
            })
            .catch(handleError)
        })
      })

      describe('currentUser', () => {
        it('should return the user owner the token', () => {
          const body = {
            query: `
              query {
                currentUser {
                  name
                  email
                }
              }
            `
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              const currentUser = response.body.data.currentUser

              expect(currentUser).to.be.an('object')
              expect(currentUser).to.have.keys(['name', 'email'])
            })
            .catch(handleError)
        })
      })
    })
  })

  describe('Mutations', () => {
    describe('application/json', () => {
      describe('createUser', () => {
        it('should create new user', () => {
          const body = {
            query: `
              mutation createNewUser($input: UserCreateInput!) {
                createUser(input: $input) {
                  name
                  email
                }
              }
            `,

            variables: {
              input: {
                name: 'Drax',
                email: 'draw@guardians.com',
                password: '123'
              }
            }
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              const createdUser = response.body.data.createUser

              expect(createdUser).to.be.an('object')
              expect(createdUser.name).to.equal('Drax')
              expect(createdUser.email).to.equal('draw@guardians.com')
            })
            .catch(handleError)
        })
      })

      describe('updateUser', () => {
        it('should update an existing user', () => {
          const body = {
            query: `
              mutation updateExistingUser($input: UserUpdateInput!) {
                updateUser(input: $input) {
                  name
                  email
                }
              }
            `,

            variables: {
              input: {
                name: 'Star lord',
                email: 'starlord@guardians.com',
                photo: 'some_photo_base64'
              }
            }
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              const updatedUser = response.body.data.updateUser

              expect(updatedUser).to.be.an('object')
              expect(updatedUser.name).to.equal('Star lord')
              expect(updatedUser.email).to.equal('starlord@guardians.com')
              expect(updatedUser.photo).to.not.be.null
              expect(updatedUser.id).to.be.undefined
            })
            .catch(handleError)
        })

        it('should block operation if token is invalid', () => {
          const body = {
            query: `
              mutation updateExistingUser($input: UserUpdateInput!) {
                updateUser(input: $input) {
                  name
                  email
                }
              }
            `,

            variables: {
              input: {
                name: 'Star lord',
                email: 'starlord@guardians.com',
                photo: 'some_photo_base64'
              }
            }
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', 'Bearer INVALID_TOKEN')
            .send(JSON.stringify(body))
            .then(response => {
              expect(response.body.data.updateUser).to.be.null
              expect(response.body).to.be.keys(['data', 'errors'])
              expect(response.body.errors).to.be.an('array')
              expect(response.body.errors[0].message).to.be.equal('JsonWebTokenError: jwt malformed')
            })
            .catch(handleError)
        })
      })

      describe('updateUserPassword', () => {
        it('should update the password of an existing user', () => {
          const body = {
            query: `
              mutation updateUserPassword($input: UserUpdatePasswordInput!) {
                updateUserPassword(input: $input)
              }
            `,

            variables: {
              input: {
                password: '1234'
              }
            }
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              expect(response.body.data.updateUserPassword).to.be.true
            })
            .catch(handleError)
        })
      })

      describe('deleteUser', () => {
        it('should delete an existing user', () => {
          const body = {
            query: `
              mutation {
                deleteUser
              }
            `
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .set('authorization', `Bearer ${token}`)
            .send(JSON.stringify(body))
            .then(response => {
              expect(response.body.data.deleteUser).to.be.true
            })
            .catch(handleError)
        })

        it('should block operation if token is not provided', () => {
          const body = {
            query: `
              mutation {
                deleteUser
              }
            `
          }

          return chai.request(app)
            .post('/graphql')
            .set('content-type', 'application/json')
            .send(JSON.stringify(body))
            .then(response => {
              expect(response.body.errors[0].message).to.equal('Unauthorized! Token not provided.')
            })
            .catch(handleError)
        })
      })
    })
  })
})
