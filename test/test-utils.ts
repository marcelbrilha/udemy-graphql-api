import * as chai from 'chai'
import app from './../src/app'
import db from './../src/models'

const chaiHttp = require('chai-http')
const expect = chai.expect
const handleError = error => {
  const message: string = (error.message) ? error.response.res.text : error.message || error
  return Promise.reject(`${error.name}: ${message}`)
}

chai.use(chaiHttp)

export {
  app,
  db,
  chai,
  expect,
  handleError
}
